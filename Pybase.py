

from os import listdir
from time import sleep
from os import remove
connected=[]

'''
Pybase is an easier way to work with files 
which is designed to help my lerning process 
of python. You can see all the usages of 
functions with help() method!!
Thank you for choosing pybase v0.1!!
Muhammet Fazıl BALIKÇI 8.07.2020
'''







def create(path:str,name:str,*columns):
    """
    Creates a new table file in the given directory
    named with given name variable.
    Also creates columns according to what given
    """
    if columns:
        try:
            if path[-1] == "\\":
                path=path[:-1]

            if name+".txt" in listdir(path):
                raise FileExistsError("Cannot delete the same named file!!")

            with open(r"{}\{}.txt".format(path,name),"w") as file:
                file.write("~".join(list(columns)))

            return 0


        except NotADirectoryError or FileNotFoundError:
            raise NotADirectoryError("Invalid path name!!")

        except TypeError:
            raise TypeError("Path,name and the column names must be a string not an other type!!")


        else:
            raise NameError("Given Name is not valid for a table name!!")

    else:
        raise NameError("Columns must include at least 1 argument, 0 were given!!")


if True:
    def connect(path:str,table:str):
        """
        Connects to the given table!!
        Must be assigned to a variable.
        """



        try:
            if path[-1]=="\\":
                path=path[:-1]
            connected.append((path,table))
            print("Connected!! ")
            return 0

        except FileNotFoundError:
            raise  FileNotFoundError("No such table!!")

        except NotADirectoryError:
            raise NotADirectoryError("No such path!!")

        except TypeError:
            raise TypeError("Path and table names must be a string not an other type!!")






    def addColumn(path:str,table:str,column_name:str):
        ###### ddeğişkenlere kaydedildiği için bu open() olayları ona göre connected değişkenler içinde ra ona göre hallet!!
        ### connected listesinde tuple[0]==path name tuple[1]==table name şekşinde store ediliyorlar!!
        """
        Adds new columns to tables, new columns will be given 0 as values by default.
        To change that, use the changeValue function!!

        """


        if (path,table) in connected or (path[:-1],table) in connected:
            if path[:-1][-1]!="\\": path=path[:-1]
            with open(r"{}\{}".format(path,table)) as file:
                data=file.read().split("\n")
                if  column_name in data[0].split("~"):
                    raise ValueError("That column name was already given!!")
                data[0]+=f"~{column_name}"
                for i in data:
                    i+="~0"

                data[0]=data[0][:-2]
            with open(r"{}\{}".format(path,table),"w") as file:
                for i in data:
                    file.write(i+"\n")

                return 0



        else:
            raise FileNotFoundError("No such file or table!!")



    def delColumn(path:str,table:str,column_name:str):
        """
        Dels the speciefied column from the specified table.

        """
        if (path,table) in connected or (path[:-1],table) in connected:
            if path[:-1][-1]!="\\": path=path[:-1]
            try:
                with open(r"{}\{}".format(path,table)) as file:

                    data=file.read().split("\n")

                    for i in data[0].split("~"):
                        if i==column_name:

                            for ii in data[0:]:
                                for iii in ii.split("~"):
                                    if ii.split("~").index(iii)==data[0].split("~").index(i):
                                        ii="~".join(ii.split("~").remove(ii.split("~")[ii.split("~").index(iii)]))
                                        break

                            data[0] = "~".join(data[0].split("~").remove(i))


                    with open(r"{}\{}".format(path,table),"w") as file:

                        file.write("\n".join(data))







            except:
                    raise ValueError("No such column!!")


        else:
            raise FileNotFoundError("No such file or table!!")

        return 0

def delTable(path:str,table:str):
    """
    Dels directly the table!!

    """
    if (path, table) in connected or (path[:-1], table) in connected:
        if path[:-1][-1] != "\\": path = path[:-1]
        remove(r"{}\{}".format(path,table))
        connected.discard(path,table)
        return 0



    else:
        raise FileNotFoundError("No such file or table!!")



def disconnect(path:str,table:str):
    """
    To avoid over-ram-using clean nonsense tables!!

    """
    if (path, table) in connected or (path[:-1], table) in connected:
        if path[:-1][-1] != "\\": path = path[:-1]

        connected.discard((path,table))
        return 0



    else:
        raise FileNotFoundError("No such file or table!!")


def changeValue(path:str,table:str,columnName:str,newValueName:str,changingLineNumber:int):
    """
    Assigns a new variable to the given column

    """
    if (path, table) in connected or (path[:-1], table) in connected:
        if path[:-1][-1] != "\\": path = path[:-1]
        with open(r"{}\{}".format(path,table)) as file:
            data=file.read().split("\n")

        try:
            for i in data[0].split("~"):
                if i==columName:
                    value=data[0].split("~").index(i)
                    break



            data[changingLineNumber-1]=data[changingLineNumber-1].split("~")
            data[changingLineNumber-1][value]=newValueName
            with open(r"{}\{}".format(path,table),"w"):
                for i in data:
                    if not i==data[changingLineNumber-1]:
                        file.write(i+"\n")
                        continue

                    file.write("~".join(i)+"\n")

        except ValueError:
            raise ValueError("Wrong type used for given parameter!!")


        except NameError:
            raise ValueError(f"That table has no attribitue {columnName}")




        return 0



    else:
        raise FileNotFoundError("No such file or table!!")






def delValue(path:str,table:str,columnName:str,changingLineNumber:int):
    """
    Dells the certain value for spesified object!!
    
    
    """

    if (path, table) in connected or (path[:-1], table) in connected:
        if path[:-1][-1] != "\\": path = path[:-1]
        with open(r"{}\{}".format(path,table)) as file:
            data=file.read().split("\n")

        try:
            for i in data[0].split("~"):
                if i==columName:
                    value=data[0].split("~").index(i)
                    break



            data[changingLineNumber-1]=data[changingLineNumber-1].split("~")
            data[changingLineNumber-1][value]=""
            with open(r"{}\{}".format(path,table),"w"):
                for i in data:
                    if not i==data[changingLineNumber-1]:
                        file.write(i+"\n")
                        continue

                    file.write("~".join(i)+"\n")

        except ValueError:
            raise ValueError("Wrong type used for given parameter!!")


        except NameError:
            raise ValueError(f"That table has no attribitue {columnName}")




        return 0



    else:
        raise FileNotFoundError("No such file or table!!")



def addObject(path:str,table:str):
    """
    Adds a new object to spesified table.
    All the values assigned with 0 by default,
    you can them with changeValue function!!

    """

    if (path, table) in connected or (path[:-1], table) in connected:
        if path[:-1][-1] != "\\": path = path[:-1]

        with open(r"{}\{}".format(path,table)) as file:
            data=file.read().split("\n")
            if  "~" not in data[-1]: data.remove(data[-1])
            data.append("~".join(["0" for x in range(data[0].count("~")+1)]))
        with open(r"{}\{}".format(path,table),"w") as file:
            print("\n".join(data),file=file)



        return 0



    else:
        raise FileNotFoundError("No such file or table!!")





def view_table(path:str,table:str):
    """
    Returns the whole table!!

    """


    if (path, table) in connected or (path[:-1], table) in connected:
        if path[:-1][-1] != "\\": path = path[:-1]

        with open(r"{}\{]".format(path,table)) as file:
            data=file.read().split("\n")
            for i in data:
                data[i]="--".join(data[i].split("~"))



        return ("\n".join(data))



    else:
        raise FileNotFoundError("No such file or table!!")



def view_object(path:str,table:str,lineNumber:int):
    """
    Returns the certain object's values!!
    """



    if (path, table) in connected or (path[:-1], table) in connected:
        if path[:-1][-1] != "\\": path = path[:-1]
        try:
            with open(r"{}\{]".format(path, table)) as file:
                data = file.read().split("\n")
                for i in data:
                    if data.index(i)+1==lineNumber:

                        data[i] = "--".join(data[i].split("~"))
                        break
            return ("--".join(data[i]))
        except:
            raise ValueError("lineNumber must be an int, not str!!")


    else:
        raise FileNotFoundError("No such file or table!!")


if __name__=="__main__":
    print("Thank You For Choosing PyBase v0.1")
    sleep(2)


del listdir
del sleep
del remove